"nodejs version 4.2.6"
"body-parser version ^1.15.0",
"express version ^4.13.4",
"mongoose version ^4.4.6"

> run the database by using command "mongod"
> go to the personal_info directory and start the nodeJS server by using command "node server.js"
> import the database for testing (visit localhost:3000/import)

You can test the API by using POSTMAN (chrome-extension) and cURL (command-line)

1. GET all ID
    Visit localhost:3000/persons
2. GET by ID
    Visit localhost:3000/____id____
3. POST
    curl -i -X POST -H 'Content-Type: application/json' -d '{ "name" : "New data", "exp" : 0, "salary" : 0, "job" : { } }' http://localhost:3000/persons
4. PUT
    curl -i -X PUT -H "Content-Type: application/json" -d "{\"name\": \"test\"}" http://localhost:3000/persons/_____id_____ 
5. DELETE
    curl -i -X DELETE http://localhost:3000/persons/_____id_____ 
