var mongoose = require('mongoose'),
Person = mongoose.model('Person');

exports.findAll = function(req, res){
    Person.find({},function(err, results) {
        return res.send(results);
    });
};

exports.findById = function(req, res){
    var id = req.params.id;
    Person.findOne({'_id':id},function(err, result) {
        return res.send(result);
    });
};

exports.add = function(req, res) {
    Person.create(req.body, function (err, person) {
    if (err) return console.log(err);
    console.log('New data is added');
    return res.send(person);
    });
}

exports.update = function(req, res) {
    var id = req.params.id;
    var updates = req.body;

    Person.update({"_id":id}, req.body,
    function (err) {
        if (err) return console.log(err);
        console.log('Updated ID:', id);
        return res.sendStatus(202);
    });
}

exports.delete = function(req, res){
    var id = req.params.id;
    Person.remove({'_id':id},function(result) {
        console.log('Deleted ID:', id);
        return res.send(result);
    });
};

exports.import = function(req, res){
      Person.create(
            {   
                "name" : "John Doe",
                "exp" : 500,
                "salary" : 60000,
                "job" : [{ 
                    "company" : {
                        "name" : "ABC Corp.",
                        "id" : "0000",
                        "size" : "big"
                    },
                    "title" : "programmer",
                    "sdate" : {
                        "month" : 1,
                        "year" : 2012,
                    },
                    "iscurrent" : false,
                    "edate" : {
                        "month" : 5,
                        "year" : 2014,
                    }},
                    { 
                    "company" : {
                        "name" : "XYZ Corp.",
                        "id" : "0002",
                        "size" : "super big"
                    },
                    "title" : "project manager",
                    "sdate" : {
                        "month" : 7,
                        "year" : 2014,
                    },
                    "iscurrent" : true,
                    "edate" : {
                        "month" : 0,
                        "year" : 0,
                    }
                }] 
            },
            {   
                "name" : "Jane Smith",
                "exp" : 0,
                "salary" : 0,
                "job" : { 
                    
                } 
            },
            {   
                "name" : "No name",
                "exp" : 5000,
                "salary" : 6000,
                "job" : { 
                    "company" : {
                        "name" : "Some company",
                        "id" : "0001",
                        "size" : "small"
                    },
                    "title" : "janitor",
                    "sdate" : {
                        "month" : 2,
                        "year" : 2010,
                    },
                    "iscurrent" : true,
                    "edate" : {
                        "month" : 0,
                        "year" : 0,
                    }
                } 
            }
      , function (err) {
            if (err) return console.log(err);
            return res.sendStatus(202);
      });
};