var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var personSchema = new Schema({
    name : String,
    exp : Number,
    salary : Number,
    job : [{ 
        company : {
            name : String,
            id : String,
            size : String
        },
        title : String,
        sdate : {
            month : Number,
            year : Number,
        },
        iscurrent : Boolean,
        edate : {
            month : Number,
            year : Number,
        }
    }]
});

mongoose.model('Person', personSchema);
